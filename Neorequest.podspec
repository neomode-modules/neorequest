Pod::Spec.new do |s|

  s.name         = "Neorequest"
  s.version      = "2.6.0"
  s.summary      = "A simple wrapper around SwiftHTTP for a cleaner syntax and some extras"
  s.description  = "A network request framework built on top of SwiftHTTP to easier the management of network requests. The framework add a cleaner syntax and some extras like managing the authorization of the requests automatically or adding a loading to a view automatically"

  s.homepage     = "http://www.neomode.com.br"
  s.license      = { :type => "Apache License, Version 2.0", :file => "LICENSE" }
  s.author    = "Neomode"
  s.platform     = :ios, "10.0"
  s.source       = { :git => "https://gitlab.com/neomode-modules/neorequest", :tag => s.version.to_s }

  s.source_files  = "Neorequest", "Neorequest/**/*.{h,m}"
  s.dependency 'SwiftHTTP'
  s.dependency 'Locksmith'
  s.dependency 'PKHUD'

  s.swift_version = '5.0'

  #s.prepare_command = 'fix_pkhub.rb'

end
