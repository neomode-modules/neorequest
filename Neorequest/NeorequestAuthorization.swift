//
//  NeorequestAuthorization.swift
//  Neorequest
//
//  Created by Edison Santiago on 02/05/17.
//  Copyright © 2017 Neomode. All rights reserved.
//

import Foundation
import Locksmith
import os.log

public enum NeorequestAuthorizationType: String {
    case bearer
    case basic
    case custom
}

public class NeorequestAuthorization {
    public var authType: NeorequestAuthorizationType = .bearer
    
    public var tokenString: String?
    public var tokenExpirationDate: Date?
    public var tokenUsageTime: TimeInterval = 1800 //Half an hour
    public var grantType: String?
    public var refreshToken: String?
    
    public var account: String?
    public var password: String?
    
    public init() {
    }
    
    /// A handler used by BEARER authentication that holds all the token regeneration logic. The handler receives the singleton NeorequestAuthorization object that should be updated with the new data
    static public var regenerateBearerTokenHandler: ((NeorequestAuthorization, NeorequestConfiguration, @escaping ((NeorequestError) -> Void), @escaping (() -> Void)) -> Void)!
    
    /// A handler used by CUSTOM authentication that holds all the token regeneration logic. The handler receives the singleton NeorequestAuthorization object that should be updated with the new data
    static public var regenerateCustomTokenHandler: ((NeorequestAuthorization, NeorequestConfiguration, @escaping ((NeorequestError) -> Void), @escaping (() -> Void)) -> Void)!
    
    /// The handler used to create a header string for custom auth. This logic must check if the token is still valid, it we must regenerate it, if the data is set. This handler MUST NOT renew the token.
    static public var getCustomValidAuthHeaderStringHandler: ((NeorequestAuthorization, NeorequestConfiguration, @escaping ((NeorequestError) -> Void), @escaping ((String) -> Void)) -> Void)!
    
    /// A optional handler used by getValidHeaders when we have more things to do besides the authorization itself
    /// We CAN assume that we will only get to this handler when we have a valid token, so we can use the token inside it
    static public var getValidCustomHeadersHandler: ((NeorequestAuthorization, NeorequestConfiguration, @escaping ((NeorequestError) -> Void), @escaping (([String:String]) -> Void)) -> Void)?
}

extension NeorequestAuthorization {
    public var headerString: String {
        get {
            switch self.authType {
            case .bearer:
                return "Bearer " + (self.tokenString!)
            case .basic:
                let loginString = String(format: "%@:%@", account!, password!)
                let loginData = loginString.data(using: String.Encoding.utf8)!
                let base64LoginString = loginData.base64EncodedString()
                return "Basic \(base64LoginString)"
            case .custom:
                //This login won't be used with the custom auth
                //Since we need to implement the handler it makes sense to put it all there
                return ""
            }
        }
    }
    
    //This method will give us ONLY the "Authorization" header string, which we will need to set on it later
    public func getValidAuthHeaderString(
        config: NeorequestConfiguration,
        onError: @escaping ((NeorequestError) -> Void),
        onSuccess: @escaping ((String) -> Void)) {
        switch self.authType {
        case .bearer:
            guard let tokenExpiration = self.tokenExpirationDate,
                tokenExpiration.timeIntervalSince(Date()) > self.tokenUsageTime else {
                    //Need to renew token
                    os_log("Token expiring soon. Needs to regenerate!", log: NeorequestLog.authorizationLogger, type: .info)
                    self.regenerateBearerToken(
                    config: config,
                    onError: onError) {
                        onSuccess(self.headerString)
                    }
                    return
            }
            
            //Token is already valid!
            onSuccess(self.headerString)
        case .basic:
            if self.account != nil, self.password != nil {
                onSuccess(self.headerString)
            }
            else {
                onError(NeorequestError.AuthorizationError(authorization: self))
            }
        case .custom:
            self.getCustomValidAuthHeaderString(config: config, onError: onError, onSuccess: onSuccess)
        }
    }
    
    //This method will give us COMPLETE headers, including the custom ones
    public func getValidHeaders(
        config: NeorequestConfiguration,
        onError: @escaping ((NeorequestError) -> Void),
        onSuccess: @escaping (([String:String]) -> Void)) {
        
        //
        var customHeaders = [String:String]()
        
        //First of all we need to generate the valid authorization header
        self.getValidAuthHeaderString(
            config: config,
            onError: onError,
            onSuccess: {
                authHeader in
                //Save the auth header
                customHeaders["Authorization"] = authHeader
                
                self.getValidCustomHeaders(
                    config: config,
                    onError: onError,
                    onSuccess: {
                        headers in
                        
                        //Add all custom headers to the object
                        for (key, value) in headers {
                            customHeaders[key] = value
                        }
                        
                        //Headers finished
                        onSuccess(customHeaders)
                })
        })
        
    }
    
    /**
     Force the expiration of a token by changing it's expiration date to now
    */
    
    
    public func expirateToken() {
        self.tokenExpirationDate = Date()
        self.save()
    }
    
    /**
     Needed by: CUSTOM Auth
     
     Check in here if the token is valid, if we need to regenerate it, etc
     
     \brief Brief Text getCustomValidAuthHeaderString
     */
    public func getCustomValidAuthHeaderString(
        config: NeorequestConfiguration,
        onError: @escaping ((NeorequestError) -> Void),
        onSuccess: @escaping ((String) -> Void)) {
        
        NeorequestAuthorization.getCustomValidAuthHeaderStringHandler(self, config, onError, onSuccess)
    }
    
    public func regenerateBearerToken(
        config: NeorequestConfiguration = NeorequestConfiguration(),
        onError: @escaping ((NeorequestError) -> Void),
        onSuccess: @escaping (() -> Void)) {
        
        NeorequestAuthorization.regenerateBearerTokenHandler(self, config, onError, onSuccess)
    }
    
    public func regenerateCustomToken(
        config: NeorequestConfiguration,
        onError: @escaping ((NeorequestError) -> Void),
        onSuccess: @escaping (() -> Void)) {
        
        NeorequestAuthorization.regenerateCustomTokenHandler(self, config, onError, onSuccess)
    }
    
    private func getValidCustomHeaders(
        config: NeorequestConfiguration,
        onError: @escaping ((NeorequestError) -> Void),
        onSuccess: @escaping (([String:String]) -> Void)) {
        
        if let customHeadersHandler = NeorequestAuthorization.getValidCustomHeadersHandler {
            customHeadersHandler(self, config, onError, onSuccess)
        }
        else {
            onSuccess([:])
        }
    }
    
}

extension NeorequestAuthorization {
    private static var keychainDefaultAccount: String {
        get {
            return "NeorequestKeychainDefaultAccount"
        }
    }
    
    func toDictionary() -> [String:Any] {
        var dataDict = [String:Any]()
        dataDict["authType"] = self.authType.rawValue
        dataDict["tokenString"] = self.tokenString
        dataDict["tokenExpirationDate"] = self.tokenExpirationDate
        dataDict["grantType"] = self.grantType
        dataDict["refreshToken"] = self.refreshToken
        dataDict["account"] = self.account
        dataDict["password"] = self.password
        
        return dataDict
    }
    
    @discardableResult public func save(userAccount account: String? = nil) -> Bool {
        var userAccount: String
        if let acc = account {
            userAccount = acc
        }
        else {
            userAccount = NeorequestAuthorization.keychainDefaultAccount
        }
        
        do {
            try Locksmith.saveData(data: self.toDictionary(), forUserAccount: userAccount)
            return true
        }
        catch LocksmithError.duplicate {
            do {
                try Locksmith.deleteDataForUserAccount(userAccount: userAccount)
                try Locksmith.saveData(data: self.toDictionary(), forUserAccount: userAccount)
                return true
            }
            catch {
                os_log("%{private}@", log: NeorequestLog.authorizationLogger, type: .error, "Error while saving to Keychain: \(error)")
                return false
            }
        }
        catch {
            os_log("%{private}@", log: NeorequestLog.authorizationLogger, type: .error, "Error while saving to Keychain: \(error)")
            return false
        }
    }
    
    @discardableResult public func delete(userAccount account: String? = nil) -> Bool {
        var userAccount: String
        if let acc = account {
            userAccount = acc
        }
        else {
            userAccount = NeorequestAuthorization.keychainDefaultAccount
        }
        do {
            try Locksmith.deleteDataForUserAccount(userAccount: userAccount)
            return true
        }
        catch {
            os_log("%{private}@", log: NeorequestLog.authorizationLogger, type: .error, "Error while deleting from Keychain: \(error)")
            return false
        }
    }
    
    public static func load(userAccount account: String? = nil) -> NeorequestAuthorization? {
        var userAccount: String
        if let acc = account {
            userAccount = acc
        }
        else {
            userAccount = NeorequestAuthorization.keychainDefaultAccount
        }
        
        if let dict = Locksmith.loadDataForUserAccount(userAccount: userAccount) {
            let auth = NeorequestAuthorization()
            auth.authType = NeorequestAuthorizationType(rawValue: dict["authType"] as? String ?? "bearer") ?? .bearer
            auth.tokenString = dict["tokenString"] as? String
            auth.tokenExpirationDate = dict["tokenExpirationDate"] as? Date
            auth.grantType = dict["grantType"] as? String
            auth.refreshToken = dict["refreshToken"] as? String
            auth.account = dict["account"] as? String
            auth.password = dict["password"] as? String
            return auth
        }
        else {
            return nil
        }
    }
}
