//
//  NeorequestLoading.swift
//  Neorequest
//
//  Created by Edison Santiago on 21/11/18.
//

import UIKit

/// The loading types available to be used in a request
public enum NeorequestLoading {
    /// No loading will be shown
    case none
    /// The entire contents of the viewController will be hidden behind a view with an `UIActivityIndicator` while the request is performed
    case fullScreen(loadingViewController: UIViewController, configuration: FullScreenLoadingConfiguration?)
    /// A loading "alert" will be shown (using `PKHUD`) and the contents of the app will be dimmed while the request is performed
    case overScreen
    /// Two closures to be called when the request starts and finishes
    case custom(start: (() -> Void), finish: (() -> Void))
}

extension NeorequestLoading {
    /// Some settings to the "loading view" created
    public struct FullScreenLoadingConfiguration {
        /// The color of the `UIActivityIndicator`. Defaults to UIColor.lightGray
        public var loadingIconColor: UIColor? = nil
        
        /// (iOS 11.0+) Should we align the loadingView to the safeArea (`true`) or to the superview (`false`)? Defaults to `true`.
        public var aligningToSafeArea: Bool? = nil
        
        /// The messages to be displayed along the loading indicator. Default: No messages.
        public var messages: [FullScreenLoadingMessage] = [FullScreenLoadingMessage]()
        
        public init(loadingIconColor: UIColor? = nil, aligningToSafeArea: Bool? = nil, messages: [FullScreenLoadingMessage] = [FullScreenLoadingMessage]()) {
            self.loadingIconColor = loadingIconColor
            self.aligningToSafeArea = aligningToSafeArea
            self.messages = messages
        }
    }
}

extension NeorequestLoading.FullScreenLoadingConfiguration {
    public struct FullScreenLoadingMessage {
        ///The text to display on the loading label
        var text: String
        ///The request time passed before we display this message. Default: 0
        var delay: TimeInterval
        
        public init(text: String, delay: TimeInterval = 0.0) {
            self.text = text
            self.delay = delay
        }
    }
}

public extension UIView {
    /**
     Adds an view with an UIActivityIndicatorView over the specified view.
     
     Sometimes we need to hide the entire contents of a view while the data is being loaded. This method intends to simplify this process by creating a "overlay" view with an `UIActivityIndicatorView`.
     
     - Important:
         - When the `.fullScreen` loading is specified in the request configuration `Neorequest` will create the loading view and remove it when the request finishes.
         - The background color of the overlay view is by default the same as the base view (in order to hide all of the view contents).
         - Only large indicators are supported.
     
     - Parameters:
         - config: (optional) Some settings for the loading view. If none is provided the default values will be used.
     
     - Returns: The reference to the overlay view created so you can remove it when the loading is finished.
     */
    func addLoadingView(withConfiguration config: NeorequestLoading.FullScreenLoadingConfiguration? = nil) -> UIView {
        let loadingIconColor = config?.loadingIconColor ?? UIColor.lightGray
        let aligningToSafeArea = config?.aligningToSafeArea ?? true
        let loadingMessages = config?.messages ?? [NeorequestLoading.FullScreenLoadingConfiguration.FullScreenLoadingMessage]()
        
        //Create loadingView
        let loadingView = UIView(frame: CGRect.zero)
        loadingView.translatesAutoresizingMaskIntoConstraints = false
        loadingView.backgroundColor = self.backgroundColor
        
        //Create stackView
        let stackView = UIStackView(frame: CGRect.zero)
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.distribution = .fill
        stackView.spacing = 10
        
        //Create activity indicator
        let activityIndicator = UIActivityIndicatorView(style: .whiteLarge)
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        activityIndicator.color = loadingIconColor
        activityIndicator.startAnimating()
        
        //Create activity indicator wrapper view
        let activityIndicatorWrapperView = UIView(frame: .zero)
        activityIndicatorWrapperView.translatesAutoresizingMaskIntoConstraints = false
        activityIndicatorWrapperView.backgroundColor = .clear
        activityIndicatorWrapperView.addSubview(activityIndicator)
        activityIndicatorWrapperView.heightAnchor.constraint(equalTo: activityIndicator.heightAnchor, multiplier: 1).isActive = true
        activityIndicator.centerXAnchor.constraint(equalTo: activityIndicatorWrapperView.centerXAnchor).isActive = true
        activityIndicator.centerYAnchor.constraint(equalTo: activityIndicatorWrapperView.centerYAnchor).isActive = true
        
        //Create label
        let loadingMessageLabel = UILabel(frame: CGRect.zero)
        loadingMessageLabel.translatesAutoresizingMaskIntoConstraints = false
        loadingMessageLabel.font = UIFont.preferredFont(forTextStyle: .callout)
        loadingMessageLabel.numberOfLines = 0
        loadingMessageLabel.textColor = loadingIconColor
        loadingMessageLabel.textAlignment = .center
        loadingMessageLabel.alpha = 0.0
        
        for message in loadingMessages {
            loadingMessageLabel.setMessage(message: message)
        }
        
        //Create view hierarchy
        stackView.addArrangedSubview(activityIndicatorWrapperView)
        stackView.addArrangedSubview(loadingMessageLabel)
        
        loadingView.addSubview(stackView)
        stackView.leadingAnchor.constraint(equalTo: loadingView.leadingAnchor, constant: 40).isActive = true
        loadingView.trailingAnchor.constraint(equalTo: stackView.trailingAnchor, constant: 40).isActive = true
//        stackView.leadingAnchor.constraint(greaterThanOrEqualTo: loadingView.leadingAnchor, constant: 40).isActive = true
//        loadingView.trailingAnchor.constraint(greaterThanOrEqualTo: stackView.trailingAnchor, constant: 40).isActive = true
        
        
        stackView.centerXAnchor.constraint(equalTo: loadingView.centerXAnchor).isActive = true
        stackView.centerYAnchor.constraint(equalTo: loadingView.centerYAnchor).isActive = true
//        loadingView.addSubview(activityIndicator)
//        activityIndicator.centerXAnchor.constraint(equalTo: loadingView.centerXAnchor).isActive = true
//        activityIndicator.centerYAnchor.constraint(equalTo: loadingView.centerYAnchor).isActive = true
        
        self.addSubview(loadingView)
        
        if #available(iOS 11.0, *), aligningToSafeArea {
            loadingView.leadingAnchor.constraint(equalTo: self.safeAreaLayoutGuide.leadingAnchor).isActive = true
            loadingView.topAnchor.constraint(equalTo: self.safeAreaLayoutGuide.topAnchor).isActive = true
            loadingView.trailingAnchor.constraint(equalTo: self.safeAreaLayoutGuide.trailingAnchor).isActive = true
            loadingView.bottomAnchor.constraint(equalTo: self.safeAreaLayoutGuide.bottomAnchor).isActive = true
        } else {
            loadingView.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
            loadingView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
            loadingView.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
            loadingView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        }
        
        self.bringSubviewToFront(loadingView)
        
        return loadingView
    }
}

internal extension UILabel {
    func setMessage(message: NeorequestLoading.FullScreenLoadingConfiguration.FullScreenLoadingMessage) {
        DispatchQueue.main.asyncAfter(deadline: .now() + message.delay) {
            self.text = message.text
            self.alpha = 1.0
        }
    }
}
