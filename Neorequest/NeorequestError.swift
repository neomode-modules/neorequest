//
//  NeorequestError.swift
//  Neorequest
//
//  Created by Edison Santiago on 02/05/17.
//  Copyright © 2017 Neomode. All rights reserved.
//

import Foundation

public enum NeorequestError: Error, CustomStringConvertible {
    case BadUrl(url: String)
    case ParseError(content: String?, parseError: Error)
    case OtherError(error: Error?)
    
    case HTTPError(code: Int, data: Data)
    case HTTP400BadRequest(data: Data)
    case HTTP401Unauthorized(data: Data)
    case HTTP404NotFound(data: Data)
    
    case AuthorizationError(authorization: NeorequestAuthorization)
    
    public var data: Data? {
        switch self {
        case .HTTPError(_, let data), .HTTP400BadRequest(let data), .HTTP401Unauthorized(let data), .HTTP404NotFound(let data):
            return data
        default:
            return nil
        }
    }
    
    public var description: String {
        switch self {
        case .BadUrl(let url):
            return "BAD URL\n\(url)"
        case .ParseError(let content, let parseError):
            return "ParseError\n" + "\(parseError)\n" + "Original Content\n" + (content ?? "")
        case .OtherError(let error):
            return "OtherError\n" + "\(error!)"
        case .HTTPError(let code, let data):
            return "HTTP \(code)\n" + (String(data: data, encoding: .utf8) ?? "")
        case .HTTP400BadRequest(let data):
            return "HTTP 400 BAD REQUEST\n" + (String(data: data, encoding: .utf8) ?? "")
        case .HTTP401Unauthorized(let data):
            return "HTTP 401 UNAUTHORIZED\n" + (String(data: data, encoding: .utf8) ?? "")
        case .HTTP404NotFound(let data):
            return "HTTP 404 NOT FOUND\n" + (String(data: data, encoding: .utf8) ?? "")
        case .AuthorizationError(let authorization):
            return "AuthorizationError\n\(authorization)"
        }
    }
}
