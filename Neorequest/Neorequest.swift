//
//  Neorequest.swift
//  Neorequest
//
//  Created by Edison Santiago on 03/05/17.
//  Copyright © 2017 Neomode. All rights reserved.
//

import Foundation
import SwiftHTTP
import PKHUD
import os.log

///Configure `Neorequest`
public struct NeorequestConfiguration {
    ///The loading which will be managed by the request
    public var loading: NeorequestLoading = .none
    
    ///How many times a failed requested should be retried before failing
    ///
    ///`401 Unauthorized` errors will NOT be counted against this limit and will ALWAYS be retried after a token regeneration.
    public var numberOfRetries: Int = 0
    
    ///Controls if we already retried on a 401 error
    ///
    ///A `401 Unauthorized` error must always be retried after we regenerate the token, but must be retried only once.
    ///If we got a 401 two times in a row even with a token regeneration between the request we will probably got a 401 in all subsequent retries.
    internal var hasRegeneratedToken: Bool = false
    
    ///The request format is JSON or not? Default: `true`
    ///
    ///If sets to false the parameters will be encoded according to the following:
    ///- If it is a GET/DELETE request the parameters will be encoded as query string
    ///- If there is a file as one of the parameters they will be encoded as form-data
    ///- If none of those are true the parameters will be encoded as `application/x-www-form-urlencoded`
    public var isJsonRequest: Bool = true
    
    ///A handler which will get notified of the request progress
    public var progress: ((Float) -> Void)? = nil
    ///Change request configurations directly on the HTTP object created by `SwiftHTTP`
    public var onOperationCreated: ((HTTP) -> Void)? = nil
    ///Change the settings of the Swift Codable JSONDecoder before parsing the response
    public var jsonDecoderConfigurationHandler: ((JSONDecoder) -> JSONDecoder)?
    
    public init() {}
    
    ///This handler will be called if we got a `401 Unauthorized` when retrying a request that already failed with `401 Unauthorized` since this probably means there is something wrong with the authentication provided.
    static public var onRequestUnauthorized: ((Data) -> Void)?
}

public struct Neorequest<T: Decodable> {
    public static func GET(
        url: String,
        headers: [String:String]? = nil,
        authorization: NeorequestAuthorization? = NeorequestAuthorization.load(),
        parameters: HTTPParameterProtocol? = nil,
        configuration: NeorequestConfiguration = NeorequestConfiguration(),
        onError: @escaping ((NeorequestError) -> Void),
        onSuccess: @escaping ((T) -> Void)) {
        
        self.make(.GET,
                  url: url,
                  headers: headers,
                  authorization: authorization,
                  parameters: parameters,
                  configuration: configuration,
                  onError: onError,
                  onSuccess: onSuccess)
    }
    
    public static func POST(
        url: String,
        headers: [String:String]? = nil,
        authorization: NeorequestAuthorization? = NeorequestAuthorization.load(),
        parameters: HTTPParameterProtocol? = nil,
        configuration: NeorequestConfiguration = NeorequestConfiguration(),
        onError: @escaping ((NeorequestError) -> Void),
        onSuccess: @escaping ((T) -> Void)) {
        
        self.make(.POST,
                  url: url,
                  headers: headers,
                  authorization: authorization,
                  parameters: parameters,
                  configuration: configuration,
                  onError: onError,
                  onSuccess: onSuccess)
    }
    
    public static func PATCH(
        url: String,
        headers: [String:String]? = nil,
        authorization: NeorequestAuthorization? = NeorequestAuthorization.load(),
        parameters: HTTPParameterProtocol? = nil,
        configuration: NeorequestConfiguration = NeorequestConfiguration(),
        onError: @escaping ((NeorequestError) -> Void),
        onSuccess: @escaping ((T) -> Void)) {
        
        self.make(.PATCH,
                  url: url,
                  headers: headers,
                  authorization: authorization,
                  parameters: parameters,
                  configuration: configuration,
                  onError: onError,
                  onSuccess: onSuccess)
    }
    
    public static func PUT(
        url: String,
        headers: [String:String]? = nil,
        authorization: NeorequestAuthorization? = NeorequestAuthorization.load(),
        parameters: HTTPParameterProtocol? = nil,
        configuration: NeorequestConfiguration = NeorequestConfiguration(),
        onError: @escaping ((NeorequestError) -> Void),
        onSuccess: @escaping ((T) -> Void)) {
        
        self.make(.PUT,
                  url: url,
                  headers: headers,
                  authorization: authorization,
                  parameters: parameters,
                  configuration: configuration,
                  onError: onError,
                  onSuccess: onSuccess)
    }
    
    public static func DELETE(
        url: String,
        headers: [String:String]? = nil,
        authorization: NeorequestAuthorization? = NeorequestAuthorization.load(),
        parameters: HTTPParameterProtocol? = nil,
        configuration: NeorequestConfiguration = NeorequestConfiguration(),
        onError: @escaping ((NeorequestError) -> Void),
        onSuccess: @escaping ((T) -> Void)) {
        
        self.make(.DELETE,
                  url: url,
                  headers: headers,
                  authorization: authorization,
                  parameters: parameters,
                  configuration: configuration,
                  onError: onError,
                  onSuccess: onSuccess)
    }
    
    private static func make(
        _ method: HTTPVerb,
        url: String,
        headers customHeaders: [String:String]? = nil,
        authorization: NeorequestAuthorization? = NeorequestAuthorization.load(),
        parameters: HTTPParameterProtocol? = nil,
        configuration: NeorequestConfiguration = NeorequestConfiguration(),
        onError: @escaping ((NeorequestError) -> Void),
        onSuccess: @escaping ((T) -> Void)) {
        
        /**
         BEGIN authentication handling
         */
        
        //Save customHeaders if exist, create a new dict if not
        var headers = customHeaders ?? [String:String]()
        
        //See if we got globalHeaders and add them to the headers
        for (key, value) in Neorequest.globalHeaders {
            headers[key] = value
        }
        
        //Check if it's a json request
        /*if configuration.isJsonRequest {
//            headers["Content-Type"] = "application/json"
        }*/
        
        //Check if we need to authenticate the request
        if let auth = authorization {
            auth.getValidHeaders(
                config: configuration,
                onError: onError
            ) {
                //We now have a full header object here
                validHeaders in
                
                //Add all of them to the header object
                for (key, value) in validHeaders {
                    headers[key] = value
                }
                
                //Continue
                self.makeAuthenticatedRequest(
                    method,
                    url: url,
                    headers: headers,
                    authorization: auth,
                    parameters: parameters,
                    configuration: configuration,
                    onError: onError,
                    onSuccess: onSuccess
                )
            }
            
//            auth.getValidAuthHeaderString(
//                config: configuration,
//                onError: onError
//            ) {
//                //Success handler
//                authHeaderString in
//                headers["Authorization"] = authHeaderString
//                //Continue
//                self.makeAuthenticatedRequest(
//                    method,
//                    url: url,
//                    headers: headers,
//                    authorization: auth,
//                    parameters: parameters,
//                    configuration: configuration,
//                    onError: onError,
//                    onSuccess: onSuccess)
//            }
        }
        else {
            //Continue with the request
            self.makeAuthenticatedRequest(
                method,
                url: url,
                headers: headers,
                authorization: nil,
                parameters: parameters,
                configuration: configuration,
                onError: onError,
                onSuccess: onSuccess)
        }
        
        /**
         END authentication handling
         */
    }
    
    private static func makeAuthenticatedRequest(
        _ method: HTTPVerb,
        url: String,
        headers: [String:String]?,
        authorization: NeorequestAuthorization?,
        parameters: HTTPParameterProtocol?,
        configuration: NeorequestConfiguration = NeorequestConfiguration(),
        onError: @escaping ((NeorequestError) -> Void),
        onSuccess: @escaping ((T) -> Void)) {
        
        /**
         BEGIN start loading handling
         */
        
        //Set loading on status bar
        DispatchQueue.main.async {
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
        }
        
        //loadingView is only used if we have a fullScreen loading
        var loadingView: UIView?
        
        switch configuration.loading {
        case .fullScreen(let loadingViewController, let configuration):
            DispatchQueue.main.async {
                loadingView = loadingViewController.view.addLoadingView(withConfiguration: configuration)
            }
        case .overScreen:
            DispatchQueue.main.async {
                PKHUD.sharedHUD.userInteractionOnUnderlyingViewsEnabled = false
                HUD.show(.progress)
            }
        case .custom(let start, _):
            start()
        default:
            break
        }
        
        
        /**
         END start loading handling
         */
        
        
        /**
         Create the base url request
        */
        
        if var req = URLRequest(urlString: url, headers: headers) {
            //We must set the method before settings the parameters
            req.verb = method
        
            //For now we only establish if it's json or not.
            //We may allow custom configuration on the future, but SwiftHTTP handles everything for us now
            if let params = parameters {
                if configuration.isJsonRequest {
                    if let error = req.appendParametersAsJSON(params) {
                        onError(NeorequestError.OtherError(error: error))
                    }
                }
                else {
                    if let error = req.appendParameters(params) {
                        onError(NeorequestError.OtherError(error: error))
                    }
                }
            }
            
            
            let opt = HTTP(req)
            
            /**
             Check if it's a json request or not
             
             By default it's ALWAYS a json request, we must explicitly set it if not a json
             */
//            let requestSerializer: HTTPSerializeProtocol = configuration.isJsonRequest ? JSONParameterSerializer() : HTTPParameterSerializer()
        
//        if let opt = HTTP.New(
//                url,
//                method: method,
//                parameters: parameters,
//                headers: headers,
//                requestSerializer: requestSerializer) {
            
            opt.onFinish = {
                response in
                
                /**
                 BEGIN stop loading handling
                 */
                
                switch configuration.loading {
                case .fullScreen(_, _):
                    //If the loading is fullscreen we have a loadingView!
                    //Giving a little time to the view to present correctly before dismiss the loading
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.25) {
                        loadingView?.removeFromSuperview()
                    }
                case .overScreen:
                    DispatchQueue.main.async {
                        HUD.hide()
                    }
                case .custom(_, let finish):
                    finish()
                default:
                    break
                }
                
                //Stop loading on status bar
                DispatchQueue.main.async {
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                }
                
                /**
                 END stop loading handling
                 */
                
                //Handle errors
                if let error = response.error {
                    if response.statusCode == 401 {
                        //We retry on a 401 only if we have an authorization set on the request and not retried yet
                        if authorization != nil, !configuration.hasRegeneratedToken {
                            os_log(
                                "\n[HTTP 401]\n%{private}@",
                                log: NeorequestLog.logger,
                                type: .error,
                                String(
                                    format: "%@ %@\nRequest Headers: %@\nResponse Data: %@",
                                    "\(method)",
                                    url,
                                    (headers?.map({"\($0), \($1)"}) ?? []),
                                    String(data: response.data, encoding: .utf8) ?? ""
                                )
                            )
                            authorization?.expirateToken()
                            
                            var config = configuration
                            config.hasRegeneratedToken = true
                            
                            self.make(
                                method,
                                url: url,
                                headers: headers,
                                authorization: NeorequestAuthorization.load(),
                                parameters: parameters,
                                configuration: config,
                                onError: onError,
                                onSuccess: onSuccess
                            )
                            
                            return
                        }
                        else {
                            NeorequestConfiguration.onRequestUnauthorized?(response.data)
                            onError(NeorequestError.HTTP401Unauthorized(data: response.data))
                            return
                        }
                    }
                    
                    //Retry the request if needed
                    if configuration.numberOfRetries > 0 {
                        var config = configuration
                        config.numberOfRetries = config.numberOfRetries - 1
                        self.makeAuthenticatedRequest(
                            method,
                            url: url,
                            headers: headers,
                            authorization: authorization,
                            parameters: parameters,
                            configuration: config,
                            onError: onError,
                            onSuccess: onSuccess
                        )
                        
                        return
                    }
                    
                    //Log some info in case we got an error and are running on debug
                    os_log(
                        "\n[HTTP %{public}d]\n%{private}@",
                        log: NeorequestLog.logger,
                        type: .error,
                        (response.statusCode ?? 0),
                        String(
                            format: "%@ %@\nRequest Headers: %@\nResponse Data: %@",
                            "\(method)",
                            url,
                            (headers?.map({"\($0), \($1)"}) ?? []),
                            String(data: response.data, encoding: .utf8) ?? ""
                        )
                    )
                    
                    
                    //Check for known errors as 400, 404
                    if let statusCode = response.statusCode {
                        switch statusCode {
                        case 400:
                            onError(NeorequestError.HTTP400BadRequest(data: response.data))
                            return
                        case 404:
                            onError(NeorequestError.HTTP404NotFound(data: response.data))
                            return
                        default:
                            onError(NeorequestError.HTTPError(code: statusCode, data: response.data))
                        }
                    }
                    //Fallback: Unknown error
                    else {
                        onError(NeorequestError.OtherError(error: error))
                    }
                    return
                }
                
                //No error
                
                //Try to parse the response to the given object or array
                var decoder = JSONDecoder()
                decoder = configuration.jsonDecoderConfigurationHandler?(decoder) ?? decoder
                
                //If the response is an empty string we treat it as an empty json object
                var responseData: Data
                if response.data.count == 0 {
                    responseData = "{}".data(using: .utf8)!
                }
                else {
                    responseData = response.data
                }
                
                do {
                    let parsedObject = try decoder.decode(T.self, from: responseData)
                    onSuccess(parsedObject)
                }
                catch {
                    //Error on parse
                    
                    onError(
                        NeorequestError.ParseError(
                            content: String(data: responseData, encoding: .utf8),
                            parseError: error
                        )
                    )
                }
            }
            
            //Add progress handler
            opt.progress = configuration.progress
            
            //Call onOperationCreated handler if it exists
            configuration.onOperationCreated?(opt)
            
            //Start operation
            opt.run()
        }
        else {
            onError(NeorequestError.OtherError(error: nil))
        }
    }
}

extension Neorequest {
    static public var globalHeaders: [String:String] {
        get {
            if let dict = UserDefaults.standard.dictionary(forKey: "NeoRequestGlobalHeadersDictionary") as? [String:String] {
                return dict
            }
            else {
                return [String:String]()
            }
        }
        set {
            UserDefaults.standard.set(newValue, forKey: "NeoRequestGlobalHeadersDictionary")
            UserDefaults.standard.synchronize()
        }
    }
}

///A dummy struct to be used when the response of the request is empty.
public struct NeorequestEmptyResponse: Codable {}

enum NeorequestLog {
    internal static let logger = OSLog(subsystem: "br.com.neomode.neorequest", category: "Neorequest")
    internal static let authorizationLogger = OSLog(subsystem: "br.com.neomode.neorequest", category: "NeorequestAuthorization")
}
